#!/bin/bash
################################################################################
#                                                                              #
#                               Kyoto University                               #
#                          Okuno and Ogata laboratory                          #
#                                                                              #
################################################################################

################################## Utilities ###################################
function installed() {
	hash $1 2>/dev/null
}

function bold() {
	tput bold;echo -n $@;tput sgr0
}

function underline() {
	tput smul;echo -n $@;tput rmul
}

function center_pad() {
	word="$1"
	while [ ${#word} -lt $2 ]; do
		word="$word$3";
		if [ ${#word} -lt $2 ]; then
			word="$3$word"
		fi;
	done;
	echo "$word"
}

function blue() {
	tput setf 1;echo -n $@;tput sgr0
}

function green() {
	tput setf 2;echo -n $@;tput sgr0
}

function cyan() {
	tput setf 3;echo -n $@;tput sgr0
}

function red() {
	tput setf 4;echo -n $@;tput sgr0
}

function magenta() {
	tput setf 5;echo -n $@;tput sgr0
}

function yellow() {
	tput setf 6;echo -n $@;tput sgr0
}

############################### Functionalities ################################
current_screen=""
chosen_rasp=""
named_peers=()
unnamed_peers=()
detected_RASPs=()
JEOL_devices=()

exited=0

diff_mic_file=../data/diff/DiffMic8ch_30dB-2.bin
hiding_after_change_time=5
switching_speaker_time=15

impulse_response_name=Kappa
impulse_response_input=../data/tsp/tsp_16384_16k_8times_stereo.wav
impulse_response_output_folder=../data/impulse_response
impulse_response_frequency=16000
impulse_response_duration=
impulse_response_channels=0-7
impulse_response_azimuths=0:5:355
impulse_response_pitches=-30:15:30
impulse_response_distances=100

playback_file="$impulse_response_input"

record_name="Test recording"
record_output_folder=../data/recordings
record_frequency=${impulse_response_frequency}
record_duration=10
record_channels=0-7

duplex_name="Test recording"
duplex_input=$impulse_response_input
duplex_output_folder=../data/recordings
duplex_frequency=${impulse_response_frequency}
duplex_duration=
duplex_channels=0-7

function is_rasp_installed() {
	installed rasplay    &&
	installed rasplay2   &&
	installed rasprec    &&
	installed raspduplex &&
	installed ws_fpaa_config &&
	installed sox
}

function install_rasp() {
	d="$(pwd)"
	cd ../rasp
	./install_rasp.sh
	cd "$d"
	
	echo "$(tput setf 4)sox is needed but not installed on this system$(tput sgr0)"
	echo "$(tput setf 6)Installing sox...$(tput sgr0)"
	sudo apt-get install sox
	
	# Update the duplex and impulse response durations, that couldn't be set without sox
	update_impulse_response_duration
	update_duplex_input_duration
}

function generate_recording_filename() {
	echo "$record_output_folder/$(date +'%G.%m.%d at %H:%M:%S') - $record_name - ch $record_channels, $record_duration s, $record_frequency Hz.wav"
}

function generate_duplex_filename() {
	echo "$record_output_folder/$(date +'%G.%m.%d at %H:%M:%S') - $duplex_name - Recording of $(basename $duplex_input) - ch $duplex_channels, $duplex_duration s, $duplex_frequency Hz.wav"
}

function split_angles() {
	echo $1 | tr ':' ' '
}

function generate_impulse_response_base_folder() {
	echo "$(date +'%G.%m.%d @ %H:%M') ▪ $impulse_response_name ▪ $(basename $impulse_response_input) ▪ $(basename $diff_mic_file) ▪ ch. $impulse_response_channels ▪ $impulse_response_frequency Hz"
}

function generate_impulse_response_filename() {
	echo "$impulse_response_output_folder/$1/$impulse_response_name - radius $(printf "%i" ${2}) pitch $(printf "%i" ${3}) azimuth $(printf "%i" ${4}).wav"
}

function update_duplex_input_duration() {
	if [ -z "duplex_duration" ]; then
		duplex_duration=$(sox "$duplex_input" -n stat 2>&1 | grep -i length | sed 's/[^0-9]*\([0-9]*\.[0-9]*\)/\1/')
	fi
}

function update_impulse_response_duration() {
	if [ -z "impulse_response_duration" ]; then
		impulse_response_duration=$(sox "$impulse_response_input" -n stat 2>&1 | grep -i length | sed 's/[^0-9]*\([0-9]*\.[0-9]*\)/\1/')
	fi
}

##################################### CLI ######################################
function context_main() {
	if [[ ${#chosen_rasp} -eq 0 ]]; then
		echo "| No RASP unit chosen yet"
	else
		echo "| RASP unit: $(red $chosen_rasp)"
	fi
}

function context_rasp_configuration() {
	if [[ ${#chosen_rasp} -eq 0 ]]; then
		echo "| No RASP unit chosen yet"
	else
		echo "| RASP unit:                    $(red $chosen_rasp)"
	fi
	
	if [[ ${#diff_mic_file} -eq 0 ]]; then
		echo "| No differential microphone data file chosen yet"
	else
		echo "| Differential microphone data: $(red $diff_mic_file)"
	fi
}

function context_playback() {
	if [[ ${#chosen_rasp} -eq 0 ]]; then
		echo "| No RASP unit chosen yet"
	else
		echo "| RASP unit:     $(red $chosen_rasp)"
	fi
	if [[ ${#playback_file} -eq 0 ]]; then
		echo "| No file chosen for playback yet"
	else
		echo "| Playback file: $(red $playback_file)"
	fi
}

function context_record() {
	if [[ ${#chosen_rasp} -eq 0 ]]; then
		echo "| No RASP unit chosen yet"
	else
		echo "| RASP unit:     $(red $chosen_rasp)"
	fi
	
	if [[ ${#record_output_folder} -eq 0 ]]; then
		echo "| No output folder chosen yet"
	else
		echo "| Output folder: $(red $record_output_folder)"
	fi
	
	if [[ ${#record_duration} -eq 0 ]]; then
		echo "| No duration chosen yet"
	else
		echo "| Duration:      $(red $record_duration seconds)"
	fi
	
	if [[ ${#record_channels} -eq 0 ]]; then
		echo "| No channels chosen yet"
	else
		echo "| Channels:      $(red $record_channels)"
	fi
	
	if [[ ${#record_frequency} -eq 0 ]]; then
		echo "| No frequency chosen yet"
	else
		echo "| Frequency:     $(red $record_frequency Hertz)"
	fi
	
	if [[ ${#record_name} -eq 0 ]]; then
		echo "| No name chosen yet"
	else
		echo "| Session name:  $(red $record_name)"
	fi
	
	echo "| Sample recording path: $(red $(basename "$(generate_recording_filename)"))"
}

function context_duplex() {
	if [[ ${#chosen_rasp} -eq 0 ]]; then
		echo "| No RASP unit chosen yet"
	else
		echo "| RASP unit:     $(red $chosen_rasp)"
	fi
	
	if [[ ${#duplex_input} -eq 0 ]]; then
		echo "| No input file chosen yet"
	else
		echo "| Input:         $(red $duplex_input)"
	fi
	
	if [[ ${#duplex_output_folder} -eq 0 ]]; then
		echo "| No output folder chosen yet"
	else
		echo "| Output folder: $(red $duplex_output_folder)"
	fi
	
	if [[ ${#duplex_duration} -eq 0 ]]; then
		echo "| No duration chosen yet"
	else
		echo "| Duration:      $(red $duplex_duration seconds)"
	fi
	
	if [[ ${#duplex_channels} -eq 0 ]]; then
		echo "| No channels chosen yet"
	else
		echo "| Channels:      $(red $duplex_channels)"
	fi
	
	if [[ ${#duplex_frequency} -eq 0 ]]; then
		echo "| No frequency chosen yet"
	else
		echo "| Frequency:     $(red $duplex_frequency Hertz)"
	fi
	
	if [[ ${#duplex_name} -eq 0 ]]; then
		echo "| No name chosen yet"
	else
		echo "| Session name:  $(red $duplex_name)"
	fi
	
	echo "| Sample recording path: $(red $(basename "$(generate_duplex_filename)"))"
}

function context_impulse_responses_recording() {
	if [[ ${#chosen_rasp} -eq 0 ]]; then
		echo "| No RASP unit chosen yet"
	else
		echo "| RASP unit:     $(red $chosen_rasp)"
	fi
	
	if [[ ${#impulse_response_input} -eq 0 ]]; then
		echo "| No input file chosen yet"
	else
		echo "| Input:         $(red $impulse_response_input)"
	fi
	
	if [[ ${#impulse_response_output_folder} -eq 0 ]]; then
		echo "| No output folder chosen yet"
	else
		echo "| Output folder: $(red $impulse_response_output_folder)"
	fi
	
	if [[ ${#impulse_response_duration} -eq 0 ]]; then
		echo "| No duration chosen yet"
	else
		echo "| Duration:      $(red $impulse_response_duration seconds)"
	fi
	
	if [[ ${#impulse_response_channels} -eq 0 ]]; then
		echo "| No channels chosen yet"
	else
		echo "| Channels:      $(red $impulse_response_channels)"
	fi
	
	if [[ ${#impulse_response_frequency} -eq 0 ]]; then
		echo "| No frequency chosen yet"
	else
		echo "| Frequency:     $(red $impulse_response_frequency Hertz)"
	fi
	
	if [[ ${#impulse_response_name} -eq 0 ]]; then
		echo "| No name chosen yet"
	else
		echo "| Session name:  $(red $impulse_response_name)"
	fi
	
	if [[ ${#impulse_response_azimuths} -eq 0 ]]; then
		echo "| No azimuths chosen yet"
	else
		azimuth_specs=($(split_angles $impulse_response_azimuths))
		if [[ ${#azimuth_specs[*]} -eq 3 ]]; then
			echo "| Azimuths:      $(red From ${azimuth_specs[0]}° to ${azimuth_specs[2]}° every ${azimuth_specs[1]}°)"
		elif [[ ${#azimuth_specs[*]} -eq 1 ]]; then
			echo "| Azimuths:      $(red At ${azimuth_specs[0]}° only)"
		fi
	fi
	
	if [[ ${#impulse_response_pitches} -eq 0 ]]; then
		echo "| No pitches chosen yet"
	else
		pitch_specs=($(split_angles $impulse_response_pitches))
		if [[ ${#pitch_specs[*]} -eq 3 ]]; then
			echo "| Pitches:       $(red From ${pitch_specs[0]}° to ${pitch_specs[2]}° every ${pitch_specs[1]}°)"
		elif [[ ${#pitch_specs[*]} -eq 1 ]]; then
			echo "| Pitches:       $(red At ${pitch_specs[0]}° only)"
		fi
	fi
	
	if [[ ${#impulse_response_pitches} -eq 0 ]]; then
		echo "| No distances chosen yet"
	else
		distance_specs=($(split_angles $impulse_response_distances))
		if [[ ${#distance_specs[*]} -eq 3 ]]; then
			echo "| Distances:     $(red From ${distance_specs[0]} cm to ${distance_specs[2]} cm every ${distance_specs[1]} cm)"
		elif [[ ${#distance_specs[*]} -eq 1 ]]; then
			echo "| Distances:     $(red At ${distance_specs[0]} cm only)"
		fi
	fi
	
	echo "| Sample recording path: $(red $(basename "$(generate_impulse_response_filename)"))"
}

function new_cli_screen() {
	# Screen changing
	tput clear
	current_screen="$1"
	
	# Title printing
	title="RASP assistant | ${1}"
	echo -e "$(bold $(center_pad " $title " 80 _))"
	
	# Contextual information printing
	case $current_screen in
		"Main") context_main;;
		"RASP configuration" | "Choosing rasp" | "Installation" | "Choosing differential microphone data file" | "Custom differential microphone data file choice") context_rasp_configuration;;
		"Playback" | "Playback file choice" | "Custom playback file choice" | "Playing") context_playback;;
		"Record"  | "Record settings" | "Recording") context_record;;
		"Duplex"  | "Duplex settings" | "Duplexing") context_duplex;;
		"Impulse response" | "Impulse response settings" | "Recording impulse responses") context_impulse_responses_recording;;
		*) ;;
	esac
	
	# Spacer
	echo ""
}

function say() {
	echo -e "$(underline $(yellow "$1"))"
}

function start() {
	tput smcup
	toplevel
}

function make_sure_rasp_installed() {
	new_cli_screen "Installation"
	if ! is_rasp_installed; then
		say "The RASP library and utility programs could not be found on this system. They"
		say "are needed by RASP assistant; do you want to install them?"
		select answer in "yes" "no"; do
			case $REPLY in
				1 | yes | y) install_rasp; make_sure_rasp_installed; break;;
				2 | no  | n) echo "RASP assisant can't work in the current state; exiting..."; wait 3; exit 1;;
				*)           echo -e "$(red Couldn\'t understand your answer)";;
			esac
		done
	fi
}

function flash_RASP_FPGA_screen() {
	new_cli_screen "RASP FPGA flashing"
	echo -e $(bold $(yellow "Flashing FPGA of RASP unit at ${chosen_rasp}, with file ${diff_mic_file}"))
	echo ""
	ws_fpaa_config -i ${chosen_rasp} -f ${diff_mic_file}
	echo ""
	echo -e $(underline $(yellow $(bold "Done!")))
	sleep 3
}

function choose_rasp() {
	# Try detecting RASPs
	if [[ ${#unnamed_peers[*]} -eq 0 && ${#named_peers[*]} -eq 0 && ${#JEOL_devices[*]} -eq 0 ]]; then
		echo -e $(bold $(yellow "Please wait while I scan the sub-networks I am connected to for other devices..."))
		echo -e $(yellow "You may be asked for root privileges to obtain MAC addresses to try guessing RASPs. The scanning takes about 5 seconds.")
		#named_peers=($(for active_interface in $(ip -o -f inet addr show | sed -n 's/.*inet \(.*\) brd.*/\1/p'); do sudo nmap -sP 2>/dev/null | sed -n "N; s/Host \([a-zA-Z]*\) (\(.*\)) is up (\(.*\) latency)\.\nMAC Address: \(.*\) (\(.*\))/\"\1\t\2\t\3\t\4\t\5\"/gp"; done))
		#unnamed_peers=($(for active_interface in $(ip -o -f inet addr show | sed -n 's/.*inet \(.*\) brd.*/\1/p'); do sudo nmap -n -sP 2>/dev/null | sed -n "N; s/Host \([0-9\.]*\) is up (\(.*\) latency)\.\nMAC Address: \(.*\) (\(.*\))/\"\1\t\2\t\3\t\4\"/gp"; done))
		JEOL_devices=($(for active_interface in $(ip -o -f inet addr show | sed -n 's/.*inet \(.*\) brd.*/\1/p'); do sudo nmap -sP -n 2>/dev/null $active_interface | sed -n "N; s/Host \([0-9\.]*\) is up (\(.*\) latency)\.\nMAC Address: \(.*\) (Jeol System Technology CO\.)/\1/gp"; done))
		echo ""
	fi
	
	if [[ ${#detected_RASPs[*]} -eq 0 && ${#JEOL_devices[*]} -gt 0 ]]; then
		detected_RASPs=${JEOL_devices[@]}
	fi
	
	# Build the list of proposals
	choices=()
	if [[ ${#detected_RASPs[*]} -gt 0 ]]; then
		choices=${detected_RASPs[@]}
		choices=(${choices[@]} Other Back)
	fi
	
	# Ask user to choose or enter rasp IP
	if [[ ${#detected_RASPs[*]} -gt 0 ]]; then
		say "Please choose or enter the IP address of the RASP you wish to use:"
		
		select answer in ${choices[@]}; do
			if [[ $REPLY -eq ${#choices[*]} ]]; then
				break 2;
			elif [[ $REPLY -eq $[${#choices[*]}-1] ]]; then
				say "Please enter the IP address of the RASP you wish to use:"
				read chosen_rasp # Ask user to provide RASP IP
				echo "Set RASP unit to $chosen_rasp"
				break
			elif [[ $REPLY -lt ${#choices[*]} ]]; then
				chosen_rasp=${choices[$[$REPLY-1]]}
				echo "Set RASP unit to $chosen_rasp"
				break
			else
				echo -e "$(red Couldn\'t understand your answer)"
			fi
		done
	else
		echo -e $(yellow "Could not find any RASP unit automatically.")
		say "Please enter the IP address of the RASP you wish to use:"
		read chosen_rasp # Ask user to provide IP
	fi
	
	if [[ -n "$chosen_rasp" ]]; then
		flash_RASP_FPGA_screen
	fi
}

function make_sure_rasp_chosen() {
	if [[ ${#chosen_rasp} -eq 0 ]]; then
		choose_rasp
	fi
}

function choose_rasp_screen() {
	new_cli_screen "Choosing rasp"
	choose_rasp
}

function enter_custom_differential_microphone_data_file() {
	new_cli_screen "Custom differential microphone data file choice"
	
	echo "(If you wish a file to be detected automatically, put it somewhere in the data/diff directory)"
	say "Please enter the path to the file you wish to use for differential microphone data:"
	read diff_mic # Ask user to provide differential microphone data file
	
	if [ ! -e $diff_mic_file ]; then
		echo -e "$(red Invalid file)"
		return 1
	fi
}

function choose_differential_microphone_data_file_screen() {
	new_cli_screen "Choosing differential microphone data file"
	
	# Detect differential microphone data files
	diff_files=($(find ../data/diff -name *.bin))
	
	# Ask user to choose or enter differential microphone data file
	if [[ ${#diff_files[*]} -gt 0 ]]; then
	echo "(If you wish a file to be detected automatically, put it somewhere in the data/diff directory)"
		say "Please choose the file you wish to use for differential microphone data:"
		
		choices=()
		choices=${diff_files[@]}
		choices=(${choices[@]} Other Back)
		
		select answer in ${choices[@]}; do
			if [[ $REPLY -eq ${#choices[*]} ]]; then
				break 2;
			elif [[ $REPLY -eq $[${#choices[*]}-1] ]]; then
				enter_custom_differential_microphone_data_file
				break
			elif [[ $REPLY -lt ${#choices[*]} ]]; then
				diff_mic_file=${choices[$[$REPLY-1]]}
				echo "Set differential microphone data file to $diff_mic_file"
				break
			else
				echo -e "$(red Couldn\'t understand your answer)"
			fi
		done
	else
		enter_custom_differential_microphone_data_file
	fi
	
	if [[ -n "$chosen_rasp" ]]; then
		flash_RASP_FPGA_screen
	fi
}

function configure_rasp_screen() {
	# We have both a RASP and a file, so we're ready to playback the file
	while :; do
		new_cli_screen "RASP configuration"
		say "What do you want to do?"
		select task in "Choose a RASP unit to use"\
			           "Choose a different differential microphone data file"\
			           "Back"; do
			case $REPLY in
				1) choose_rasp_screen; break;;
				2) choose_differential_microphone_data_file_screen; break;;
				3) break 2;;
				*) echo -e "$(red Couldn\'t understand your answer)";;
			esac
		done
	done
}

function enter_custom_playback_file() {
	new_cli_screen "Custom playback file choice"
	
	echo "(If you wish a file to be detected automatically, put it somewhere in the data directory)"
	say "Please enter the path to the file you wish to play on the RASP unit:"
	read playback_file # Ask user to provide playback file
	
	if [ ! -e $playback_file ]; then
		echo -e "$(red Invalid file)"
		return 1
	fi
}

function choose_playback_file_screen() {
	new_cli_screen "Playback file choice"
	
	# Detect playable files
	IFS=$'\n'
	playable=($(find ../data -name *.wav -print -or -path ../data/impulse_response -prune))
	
	# Ask user to choose or enter rasp IP
	if [[ ${#playable[*]} -gt 0 ]]; then
		echo "(If you wish to use another file, put it somewhere in the data directory)"
		say "Please choose the file you wish to play on the RASP unit:"
		
		choices=()
		choices=(${playable[@]})
		choices=(${choices[@]} Other Back)
		
		select answer in ${choices[@]}; do
			if [[ $REPLY -eq ${#choices[*]} ]]; then
				break 2;
			elif [[ $REPLY -eq $[${#choices[*]}-1] ]]; then
				enter_custom_playback_file
				break
			elif [[ $REPLY -lt ${#choices[*]} ]]; then
				playback_file=${choices[$[$REPLY-1]]}
				echo "Set playback file to $playback_file"
				break
			else
				echo -e "$(red Couldn\'t understand your answer)"
			fi
		done	
		unset IFS
	else
		enter_custom_playback_file
	fi
}

function playing_rasp_screen() {
	new_cli_screen "Playing"
	# Play given file
	say "Playing file $playback_file on RASP unit $chosen_rasp"
	rasplay2 $chosen_rasp "$playback_file"
}

function playback_rasp_screen() {
	# Ask for a RASP if there is none yet
	new_cli_screen "Playback"
	make_sure_rasp_chosen
	
	# Choose a playback file if there is none yet
	if [[ ${#playback_file} -eq 0 ]]; then	
		new_cli_screen "Playback"
		choose_playback_file_screen
	fi
	
	# We have both a RASP and a file, so we're ready to playback the file
	while :; do
		new_cli_screen "Playback"
		say "What do you want to do?"
		select task in "Play the file now!"\
			           "Choose a different file to playback"\
			           "Back"; do
			case $REPLY in
				1) playing_rasp_screen; break;;
				2) choose_playback_file_screen; break;;
				3) break 2;;
				*) echo -e "$(red Couldn\'t understand your answer)";;
			esac
		done
	done
}

function recording_rasp_screen() {
	new_cli_screen "Recording"
	
	# Generate file name
	recording_file="$(generate_recording_filename)"
	
	# Record with set parameters
	say "Recording file  $(basename "$recording_file") from RASP unit $chosen_rasp"
	echo ""
	mkdir -p "$(dirname "$recording_file")"
	rasprec $chosen_rasp "$recording_file" $record_frequency $record_duration $record_channels
}

function enter_custom_record_folder() {
	new_cli_screen "Record settings"
	
	say "Please enter the path to the folder you wish to save recordings to:"
	read record_output_folder # Ask user to provide output record folder
	
	if [ ! -d $record_output_folder ]; then
		echo -e "The folder you specified doesn't exist; do you want to create it?"
		
		select answer in yes no; do
			case $REPLY in
				1 | yes | y) mkdir -p "$record_output_folder"; break;;
				2 | no  | n) return 1; break;;
				*) echo -e "$(red Couldn\'t understand your answer)";;
			esac
		done
	fi
}

function change_record_output_folder_screen() {
	new_cli_screen "Record settings"
	
	# Detect output directories
	folders=($(find ../data -type d))
	
	# Ask user to choose or enter rasp IP
	if [[ ${#folders[*]} -gt 0 ]]; then
		say "Please choose the folder you wish to put the recording in:"
		
		choices=()
		choices=${folders[@]}
		choices=(${choices[@]} Other Back)
		
		select answer in ${choices[@]}; do
			if [[ $REPLY -eq ${#choices[*]} ]]; then
				break 2;
			elif [[ $REPLY -eq $[${#choices[*]}-1] ]]; then
				enter_custom_record_folder
				break
			elif [[ $REPLY -lt ${#choices[*]} ]]; then
				record_output_folder=${choices[$[$REPLY-1]]}
				echo "Set recording output folder to $record_output_folder"
				break
			else
				echo -e "$(red Couldn\'t understand your answer)"
			fi
		done
	else
		enter_custom_record_folder
	fi
}

function change_record_duration_screen() {
	new_cli_screen "Record settings"
	
	say "Please enter the duration of the recording, in seconds:"
	read record_duration
}

function change_record_channels_screen() {
	new_cli_screen "Record settings"
	
	say "Please enter the channels you wish to record (e.g. 3 or 0-7):"
	read record_channels
}

function change_record_frequency_screen() {
	new_cli_screen "Record settings"
	
	say "Please enter the sampling rate of the recording, in Hertz:"
	read record_frequency
}

function change_record_session_name_screen() {
	new_cli_screen "Record settings"
	
	say "Please enter the session name for recording:"
	read record_name
}

function record_rasp_screen() {
	# Ask for a RASP if there is none yet
	new_cli_screen "Record"
	make_sure_rasp_chosen
	
	while :; do
		new_cli_screen "Record"
		say "What do you want to do?"
		select task in "Record now!"\
			           "Change the output folder"\
			           "Change the recording duration"\
			           "Change which channels are recorded"\
			           "Change the recording frequency"\
			           "Change the session name"\
			           "Back"; do
			case $REPLY in
				1) recording_rasp_screen;              break;;
				2) change_record_output_folder_screen; break;;
				3) change_record_duration_screen;      break;;
				4) change_record_channels_screen;      break;;
				5) change_record_frequency_screen;     break;;
				6) change_record_session_name_screen;  break;;
				7) break 2;;
				*) echo -e "$(red Couldn\'t understand your answer)";;
			esac
		done
	done
}

function duplexing_rasp_screen() {
	new_cli_screen "Duplexing"
	
	# Generate file name
	duplex_file="$(generate_duplex_filename)"
	
	# Record with set parameters
	say "Recording file $(basename "$duplex_file") while playing $(basename "$duplex_input") from RASP unit $chosen_rasp"
	echo ""
	mkdir -p "$(dirname "$duplex_file")"
	raspduplex $chosen_rasp "$duplex_input" "$duplex_file" $duplex_frequency $duplex_duration $duplex_channels
}

function enter_custom_duplex_input() {
	new_cli_screen "Duplex settings"
	
	echo "(If you wish a file to be detected automatically, put it somewhere in the data directory)"
	say "Please enter the path to the file you wish to play on the RASP unit:"
	read duplex_input # Ask user to provide duplex input file
	
	if [ ! -e $duplex_input ]; then
		echo -e "$(red Invalid file)"
		return 1
	fi
}

function change_duplex_input_screen() {
	new_cli_screen "Duplex settings"
	
	# Detect playable files
	playable=($(find ../data -name *.wav))
	
	# Ask user to choose or enter rasp IP
	if [[ ${#playable[*]} -gt 0 ]]; then
		echo "(If you wish to use another file, put it somewhere in the data directory)"
		say "Please choose the file you wish to play on the RASP unit:"
		
		choices=()
		choices=${playable[@]}
		choices=(${choices[@]} Other Back)
		
		select answer in ${choices[@]}; do
			if [[ $REPLY -eq ${#choices[*]} ]]; then
				break 2;
			elif [[ $REPLY -eq $[${#choices[*]}-1] ]]; then
				enter_custom_duplex_input
				break
			elif [[ $REPLY -lt ${#choices[*]} ]]; then
				duplex_input=${choices[$[$REPLY-1]]}
				update_duplex_input_duration
				echo "Set duplex input to $duplex_input"
				break
			else
				echo -e "$(red Couldn\'t understand your answer)"
			fi
		done
	else
		enter_custom_duplex_input
	fi
}

function enter_custom_duplex_folder() {
	new_cli_screen "Duplex settings"
	
	say "Please enter the path to the folder you wish to save recordings to:"
	read duplex_output_folder # Ask user to provide output duplex folder
	
	if [ ! -d $duplex_output_folder ]; then
		echo -e "$(red Invalid file)"
		return 1
	fi
}

function change_duplex_output_folder_screen() {
	new_cli_screen "Duplex settings"
	
	# Detect output directories
	folders=($(find ../data -type d))
	
	# Ask user to choose or enter rasp IP
	if [[ ${#folders[*]} -gt 0 ]]; then
		say "Please choose the folder you wish to put the recording in:"
		
		choices=()
		choices=${choices[@]}
		choices=(${choices[@]} Other Back)
		
		select answer in ${choices[@]}; do
			if [[ $REPLY -eq ${#choices[*]} ]]; then
				break 2;
			elif [[ $REPLY -eq $[${#choices[*]}-1] ]]; then
				enter_custom_duplex_folder
				break
			elif [[ $REPLY -lt ${#choices[*]} ]]; then
				duplex_output_folder=${choices[$[$REPLY-1]]}
				echo "Set recording output folder to $duplex_output_folder"
				break
			else
				echo -e "$(red Couldn\'t understand your answer)"
			fi
		done
	else
		enter_custom_duplex_folder
	fi
}

function change_duplex_duration_screen() {
	new_cli_screen "Duplex settings"
	
	say "Please enter the duration of the recording, in seconds:"
	read duplex_duration
}

function change_duplex_channels_screen() {
	new_cli_screen "Duplex settings"
	
	say "Please enter the channels you wish to record (e.g. 3 or 0-7):"
	read duplex_channels
}

function change_duplex_frequency_screen() {
	new_cli_screen "Duplex settings"
	
	say "Please enter the sampling rate of the recording, in Hertz:"
	read duplex_frequency
}

function change_duplex_session_name_screen() {
	new_cli_screen "Duplex settings"
	
	say "Please enter the session name for recording:"
	read duplex_name
}

function duplex_rasp_screen() {
	# Ask for a RASP if there is none yet
	new_cli_screen "Duplex"
	make_sure_rasp_chosen
	
	while :; do
		new_cli_screen "Duplex"
		say "What do you want to do?"
		select task in "Play and record now!"\
			           "Change the file to play"\
			           "Change the output folder"\
			           "Change the recording duration"\
			           "Change which channels are recorded"\
			           "Change the recording frequency"\
			           "Change the session name"\
			           "Back"; do
			case $REPLY in
				1) duplexing_rasp_screen;              break;;
				2) change_duplex_input_screen;         break;;
				3) change_duplex_output_folder_screen; break;;
				4) change_duplex_duration_screen;      break;;
				5) change_duplex_channels_screen;      break;;
				6) change_duplex_frequency_screen;     break;;
				7) change_duplex_session_name_screen;  break;;
				8) break 2;;
				*) echo -e "$(red Couldn\'t understand your answer)";;
			esac
		done
	done
}

function recording_impulse_responses_rasp_screen() {
	new_cli_screen "Recording impulse responses"
	
	# Extract the start, end and increment for distance, pitch and azimuth
	distance_specs=($(split_angles $impulse_response_distances))
	pitch_specs=($(split_angles $impulse_response_pitches))
	azimuth_specs=($(split_angles $impulse_response_azimuths))
	distances=($(eval echo {${distance_specs[0]}..${distance_specs[2]:-${distance_specs[0]}}..${distance_specs[1]:-1}}))
	pitches=($(eval echo {${pitch_specs[0]}..${pitch_specs[2]:-${pitch_specs[0]}}..${pitch_specs[1]:-1}}))
	azimuths=($(eval echo {${azimuth_specs[0]}..${azimuth_specs[2]:-${azimuth_specs[0]}}..${azimuth_specs[1]:-1}}))
	n_recordings=$[${#distances[*]}*${#pitches[*]}*${#azimuths[*]}]
	time_per_recording=$(echo "$switching_speaker_time+$impulse_response_duration" | bc)
	estimated_seconds=$(echo "$n_recordings*$time_per_recording" | bc | sed 's/\([0-9]*\)\.*[0-9]*/\1/')
	
	echo -e "$(yellow "Ready to record impulse response. This is a long process; assuming you can switch the speaker position every $switching_speaker_time seconds, it will take at least $(bold $(($estimated_seconds/3600)) hours, $(($estimated_seconds%3600/60)) minutes and $(($estimated_seconds%60)) seconds)") $(yellow to record the $(bold $n_recordings impulse responses))$(yellow .)"
	echo ""
	
	say "Are you sure you want to start?"
	select answer in "yes" "no"; do
		case $REPLY in
			1 | yes | y) new_cli_screen "Recording impulse responses"; 
			             echo "--------------------------------------------------------------------------------"; 
			             break;;
			2 | no  | n) return 1;;
			*)           echo -e "$(red Couldn\'t understand your answer)";;
		esac
	done
	
	# Create output folder
	base_folder="$(generate_impulse_response_base_folder)" 
	mkdir -p "$impulse_response_output_folder/$base_folder"
	
	for d in ${distances[@]}; do
		for p in ${pitches[@]}; do
			for a in ${azimuths[@]}; do
				# Generate file name
				impulse_response_file="$(echo "$(generate_impulse_response_filename "$base_folder" $d $p $a)" | sed 's/ /\ /g')"
				
				# Record with set parameters
				echo "Ready to record TSP for $(blue distance $(bold $d cm)), $(green pitch $(bold $p°)) and $(yellow azimuth $(bold $a°))"
				read -p "Press ENTER to start; you will have 5 seconds before the recording starts..."
				sleep $hiding_after_change_time
				raspduplex $chosen_rasp "$impulse_response_input" "$impulse_response_file" $impulse_response_frequency $impulse_response_duration $impulse_response_channels
			done
		done
	done
	
	echo ""
	echo -e "$(green $(bold All done!) Your impulse response recordings should all be in folder $(underline $impulse_response_output_folder/$base_folder))"
	sleep 10
}

function change_impulse_response_azimuths_screen() {
	new_cli_screen "Impulse response settings"
	
	say "Please enter the azimuths in degrees  that the source will be at successively (e.g. 0:5:360):"
	read impulse_response_azimuths
}

function change_impulse_response_pitches_screen() {
	new_cli_screen "Impulse response settings"
	
	say "Please enter the pitches in degrees that the source will be at successively (e.g. -30:15:30):"
	read impulse_response_pitches
}

function change_impulse_response_distances_screen() {
	new_cli_screen "Impulse response settings"
	
	say "Please enter the distances in cm that the source will be at successively (e.g. 100 or 100:25:200):"
	read impulse_response_distances
}

function enter_custom_impulse_response_input() {
	new_cli_screen "Impulse response settings"
	
	echo "(If you wish a file to be detected automatically, put it somewhere in the data directory)"
	say "Please enter the path to the file you wish to play on the RASP unit:"
	read impulse_response_input # Ask user to provide duplex input file
	
	if [ ! -e $impulse_response_input ]; then
		echo -e "$(red Invalid file)"
		return 1
	else
		update_impulse_response_duration
	fi
}

function change_impulse_response_input_screen() {
	new_cli_screen "Impulse response settings"
	
	# Detect playable files
	playable=($(find ../data -name *.wav))
	
	# Ask user to choose or enter rasp IP
	if [[ ${#playable[*]} -gt 0 ]]; then
		echo "(If you wish to use another file, put it somewhere in the data directory)"
		say "Please choose the file you wish to play on the RASP unit:"
		
		choices=()
		choices=${playable[@]}
		choices=(${choices[@]} Other Back)
		
		select answer in ${choices[@]}; do
			if [[ $REPLY -eq ${#choices[*]} ]]; then
				break 2;
			elif [[ $REPLY -eq $[${#choices[*]}-1] ]]; then
				enter_custom_impulse_response_input
				break
			elif [[ $REPLY -lt ${#choices[*]} ]]; then
				impulse_response_input=${choices[$[$REPLY-1]]}
				update_impulse_response_duration
				echo "Set impulse response input file to $impulse_response_file"
				break
			else
				echo -e "$(red Couldn\'t understand your answer)"
			fi
		done
	else
		enter_custom_impulse_response_input
	fi
}

function enter_custom_impulse_response_folder() {
	new_cli_screen "Impulse response settings"
	
	say "Please enter the path to the folder you wish to save impulse responses to:"
	read impulse_response_output_folder # Ask user to provide output impulse_response folder
	
	if [ ! -d $impulse_response_output_folder ]; then
		echo -e "$(red Invalid file)"
		return 1
	fi
}

function change_impulse_response_output_folder_screen() {
	new_cli_screen "Impulse response settings"
	
	# Detect output directories
	folders=($(find ../data -type d))
	
	# Ask user to choose or enter rasp IP
	if [[ ${#folders[*]} -gt 0 ]]; then
		say "Please choose the folder you wish to put the impulse responses in:"
		
		choices=()
		choices=${folders[@]}
		choices=(${choices[@]} Other Back)
		
		select answer in ${choices[@]}; do
			if [[ $REPLY -eq ${#choices[*]} ]]; then
				break 2;
			elif [[ $REPLY -eq $[${#choices[*]}-1] ]]; then
				enter_custom_impulse_response_folder
				break
			elif [[ $REPLY -lt ${#choices[*]} ]]; then
				impulse_response_output_folder=${choices[$[$REPLY-1]]}
				echo "Set impulse response output folder to $impulse_response_output_folder"
				break
			else
				echo -e "$(red Couldn\'t understand your answer)"
			fi
		done
	else
		enter_custom_impulse_response_folder
	fi
}

function change_impulse_response_channels_screen() {
	new_cli_screen "Impulse response settings"
	
	say "Please enter the channels you wish to record (e.g. 3 or 0-7):"
	read impulse_response_channels
}

function change_impulse_response_frequency_screen() {
	new_cli_screen "Impulse response settings"
	
	say "Please enter the sampling rate of the impulse responses, in Hertz:"
	read impulse_response_frequency
}

function change_impulse_response_session_name_screen() {
	new_cli_screen "Impulse response settings"
	
	say "Please enter the session name for the impulse response recordings:"
	read impulse_response_name
}

function record_impulse_responses_rasp_screen() {
	# Ask for a RASP if there is none yet
	new_cli_screen "Impulse response"
	make_sure_rasp_chosen
	
	while :; do
		new_cli_screen "Impulse response"
		say "What do you want to do?"
		select task in "Record impulse responses now!"\
			           "Change azimuths to record"\
			           "Change pitch angles to record"\
			           "Change distances to record"\
			           "Change the file to play"\
			           "Change the output folder"\
			           "Change which channels are recorded"\
			           "Change the recording frequency"\
			           "Change the session name"\
			           "Back"; do
			case $REPLY in
				1)  recording_impulse_responses_rasp_screen;      break;;
				2)  change_impulse_response_azimuths_screen;      break;;
				3)  change_impulse_response_pitches_screen;       break;;
				4)  change_impulse_response_distances_screen;     break;;
				5)  change_impulse_response_input_screen;         break;;
				6)  change_impulse_response_output_folder_screen; break;;
				7)  change_impulse_response_channels_screen;      break;;
				8)  change_impulse_response_frequency_screen;     break;;
				9)  change_impulse_response_session_name_screen;  break;;
				10) break 2;;
				*) echo -e "$(red Couldn\'t understand your answer)";;
			esac
		done
	done
}

function toplevel() {
	make_sure_rasp_installed
	while :; do
		new_cli_screen "Main"
		say "What do you want to do?"
		select task in "Choose and configure the RASP unit you want to use"\
			           "Play something"\
			           "Record something"\
			           "Play something and record simultaneously"\
			           "Record impulse responses"\
			           "Quit"; do
			case $REPLY in
				1) configure_rasp_screen; break;;
				2) playback_rasp_screen;  break;;
				3) record_rasp_screen;    break;;
				4) duplex_rasp_screen;    break;;
				5) record_impulse_responses_rasp_screen; break;;
				6) break 2;;
				*) echo -e "$(red Couldn\'t understand your answer)";;
			esac
		done
	done
}

function cleanup() {
	if [[ exited -eq 0 ]]; then
		exited=1
		tput rmcup
		echo "Done!"
		tput sgr0
	fi
}

trap "cleanup;exit" SIGINT EXIT
start

################################################################################
